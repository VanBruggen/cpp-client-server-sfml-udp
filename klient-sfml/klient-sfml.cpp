#include <SFML\Network.hpp>
#include <iostream>
#include <bitset>
#include <string>
#include <limits>

// Zmienne do odczytu danych z komunikatu, zapisu danych do komunikatu oraz obsługi operacji i odpowiedzi:
uint8_t operacja = 1;
uint8_t odpowiedz = 0;
uint8_t sesja = 0;
uint8_t liczba = 0;
bool stop = false;

// Funkcja do odczytu odebranego komunikatu:
void odczyt(const std::bitset<24> &a)
{
    /* Podzial odebranego komunikatu na 3 bajty i zlozenie ich w odwrotnej, poprawnej kolejnosci, do bitsetu "a_2",
       tak aby nie wplywac na globalny bitset do odbioru "in".  */
    std::string sub1 = a.to_string().substr(0,8);
    std::string sub2 = a.to_string().substr(8,8);
    std::string sub3 = a.to_string().substr(16,8);

    std::bitset<24> a_2(sub3 + sub2 + sub1);

    /* Utworzenie dodatkowego bitsetu "a_kopia", aby po kazdym iloczynie logicznym (w tym przypadku zaaplikowaniu
   maski bitowej) moc przywrocic mu wejsciowy stan, pobrany z bitsetu "a_2", jako ze jedyna mozliwosc wykonania
   takiego iloczynu na bitsecie wziaze sie z jego zmiana.   */
    std::bitset<24> a_kopia(a_2);

    /* Aplikowanie odpowiednich masek bitowych i stosowanie przesuniecia bitowego o odpowiednia liczbe miejsc, aby
       odczytac wartosci odpowiednich pol komunikatu:   */
    a_kopia &= 0b111100000000000000000000;
    operacja = static_cast<uint8_t>((a_kopia >> 20).to_ulong());
    a_kopia = a_2;
    a_kopia &= 0b000011100000000000000000;
    odpowiedz = static_cast<uint8_t>((a_kopia >> 17).to_ulong());
    a_kopia = a_2;
    a_kopia &= 0b000000011110000000000000;
    sesja = static_cast<uint8_t>((a_kopia >> 13).to_ulong());
    a_kopia = a_2;
    a_kopia &= 0b000000000001111111100000;
    liczba = static_cast<uint8_t>((a_kopia >> 5).to_ulong());

    // Bezposrednie odczytanie flagi stopu z komunikatu:
    a_kopia = a_2;
    stop = a_kopia.test(4);

    // Wyswietlenie odczytanych z komunikatu danych:
    std::cout << "\n\n(Odczyt) Bitset po odczycie: " << a_kopia;

    std::cout << "\n\nKod operacji: " << (int)operacja << "\nKod odpowiedzi: " << (int)odpowiedz << "\nID sesji: " << (int)sesja << "\nLiczba: " << (int)liczba << "\nFlaga stopu: " << stop;
}

// Funkcja do przygotowania komunikatu do wyslania:
void zapis(std::bitset<24> &a)
{
    // Utworzenie czterech bitsetow do przechowania wyjsciowych wartosci pol (w postaci binarnej) dla tworzonego komunikatu:
    std::bitset<4> operacja_bity(operacja);
    std::bitset<3> odpowiedz_bity(odpowiedz);
    std::bitset<4> sesja_bity(sesja);
    std::bitset<8> liczba_bity(liczba);

    // Utworzenie wyjsciowego komunikatu w postaci napisu, zlozonego z przekonwertowanych na napisy powyzszych bitsetow i z dopelnienia:
    std::string pakiet = operacja_bity.to_string() + odpowiedz_bity.to_string() + sesja_bity.to_string() + liczba_bity.to_string() + "00000";
    std::cout << "\n\nPakiet po 1. zapisie (string): " << pakiet;

    /* Podzial komunikatu w postaci napisu na 3 bajty (abstrakcyjne) i ponowne zlozenie ich w kolejnosci odpowiedniej
       do transmisji przez gniazdo oraz konwersja do bitsetu i przypisanie jego wartosci do podanego do funkcji bitsetu, sluzacego
       do wyslania komunikatu:     */
    std::string sub1 = pakiet.substr(0,8);
    std::string sub2 = pakiet.substr(8,8);
    std::string sub3 = pakiet.substr(16,8);

    a = std::bitset<24> (sub3 + sub2 + sub1);

    // Na koniec (inaczej byloby trudniej) odpowiednie ustawienie flagi stopu w podanym do funkcji bitsecie:
    if (stop) {a.set(20);} else {a.reset(20);}
}

int main()
{
    // Bitset do odbierania i wysylania komunikatow:
    std::bitset<24> komunikat;

    // Gniazdo UDP i dane interfejsu odbiorcy:
    sf::UdpSocket socket;
    sf::IpAddress receiver = "192.168.1.1";
    unsigned short port = 54000;

    // Zmienne pomocnicze, do zwracania do nich przez funkcje "receive()" IP nadawcy, portu nadawcy oraz liczby otrzymanych bajtow:
    sf::IpAddress sender;
    std::size_t received;
    unsigned short port2;

    // Lokalna zmienna do przechowywania nadanego ID sesji:
    std::string id;

    // Zmienna na liczbe inicjalizujaca:
    unsigned int initialize;

    // Zmienna pomocnicza, do lokalnej walidacji poprawnosci podanej z konsoli liczby pod wzgledem zakresu (jedyna mozliwa walidacja):
    long long licz = 0;

    // Bitset flag do obslugi roznych petli "while" programu:
    std::bitset<15> send_flag("111111111111111");

    // Przygotowanie komunikatu z zadaniem o nadanie ID sesji do wyslania (komunikat inicjalizujacy):
    zapis(komunikat);

    // Powiazanie gniazda z dowolnym wolnym portem:
    if (socket.bind(sf::Socket::AnyPort) != sf::Socket::Done)
    {
        std::cout << "\n\nBlad gniazda (bindowanie).";
    }

    // Wyslanie komunikatu z zadaniem o nadanie ID sesji:
    if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
    {
        std::cout << "\n\nBlad gniazda (wysylanie).";

    } else {std::cout << "\n\nWyslano pakiet.";}


    // Glowna petla programu. Wykonuje sie dopoki w komunikacie nie otrzyma ustawionej flagi stopu:
    while (true)
    {
        std::cout << "\n\nWaiting...";

        // Odebranie i odczyt pierwszej odpowiedzi:
        if (socket.receive(&komunikat, 3, received, sender, port2) != sf::Socket::Done)
        {
            std::cout << "\n\nBlad gniazda (odbieranie).";
        }
        odczyt(komunikat);

        if (odpowiedz == 1)
        {
            std::cout << "\n\nOtrzymano potwierdzenie.";
        }
        else if (odpowiedz == 2)
        {
            std::cout << "\n\nNiepoprawna forma lub kolejnosc komunikatu. Przerywam program.";
            return 0;
        }
        else if (odpowiedz == 3)
        {
            std::cout << "\n\nOtrzymano ID sesji.";

            // Pobieranie od uzytkownika liczby inicjalizujacej, do pierwszego sukcesu:
            bool stop11 = false;
            while (!stop11) {
                std::cout << "\nPodaj parzysta liczbe z zakresu <2 ; 254>" <<
                          "\n> ";
                std::cin >> licz;

                // Walidacja zakresu:
                if (licz < 0 || licz > 255) {

                    std::cout << "\n\nPodano liczbe spoza akceptowalnego zakresu. Sprobuj ponownie:";

                } else {

                    stop11 = true;
                }
            }

            // Zarzutowanie i przypisanie pobranej liczby do zmiennej przechowujacej liczbe inicjalizujaca:
            initialize = static_cast<unsigned int>(licz);

            // Przygotowanie komunikatu z liczba inicjalizujaca do wyslania:
            operacja = 2;
            odpowiedz = 0;
            liczba = initialize;
            zapis(komunikat);

            // Wyslanie komunikatu z liczba inicjalizujaca:
            if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
            {
                std::cout << "\n\nBlad gniazda (wysylanie).";
            }
        }
        else if (odpowiedz == 4)
        {
            // Blok kodu obslugujacy pierwsza probe odgadniecia tajnej liczby:
            bool stop_09 = false;
            std::cout << "\n\nMaksymalna liczba prob: " << (int)liczba;

            // Pobieranie od uzytkownika liczby, do pierwszego sukcesu:
            while (!stop_09) {
                std::cout << "\n\nPodaj liczbe z zakresu <1 ; 255>:" <<
                          "\n> ";
                std::cin >> licz;

                // Walidacja zakresu:
                if (licz < 0 || licz > 255) {

                    std::cout << "\n\nPodano liczbe spoza akceptowalnego zakresu. Sprobuj ponownie:";

                } else {

                    // Przypisanie pobranej liczby do zmiennej przechowujacej liczbe do wyslania w kolejnym komunikacie oraz zakonczenie petli:
                    liczba = static_cast<unsigned int>(licz);
                    stop_09 = true;
                }
            }

            // Przygotowanie pozostalych pol komunikatu bedacego pierwsza proba odgadniecia tajnej liczby:
            operacja = 3;
            odpowiedz = 0;
            zapis(komunikat);

            // Wyslanie komunikatu bedacego pierwsza proba odgadniecia tajnej liczby:
            if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
            {
                std::cout << "\n\nBlad gniazda (wysylanie).";
            }

        }
        else if (odpowiedz == 5)
        {
            // Blok kodu obslugujacy odpowiedz o niedozwolonej liczbie:
            std::cout << "\n\nPodano niedozwolona liczbe. Sprobuj ponownie.";
            if (operacja == 2)
            {
                // ...w przypadku podania liczby inicjalizujacej:
                // Pobieranie od uzytkownika liczby inicjalizujacej, do pierwszego sukcesu:
                bool stop12 = false;
                while (!stop12) {
                    std::cout << "\nPodaj parzysta liczbe z zakresu <2 ; 254>" <<
                              "\n> ";
                    std::cin >> licz;

                    // Walidacja zakresu:
                    if (licz < 0 || licz > 255) {

                        std::cout << "\n\nPodano liczbe spoza akceptowalnego zakresu. Sprobuj ponownie:";

                    } else {

                        // Zarzutowanie i przypisanie pobranej liczby do zmiennej przechowujacej liczbe inicjalizujaca oraz zakonczenie petli:
                        liczba = static_cast<unsigned int>(licz);
                        stop12 = true;
                    }
                }

                // Przygotowanie komunikatu z liczba inicjalizujaca do wyslania:
                odpowiedz = 0;
                zapis(komunikat);

                // Wyslanie komunikatu z liczba inicjalizujaca:
                if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
                {
                    std::cout << "\n\nBlad gniazda (wysylanie).";
                }
            }
            else if (operacja == 3)
            {

                // ...w przypadku podania liczby bedacej proba odgadniecia tajnej:
                // Pobieranie od uzytkownika liczby, do pierwszego sukcesu:
                bool stop14 = false;
                while (!stop14) {
                    std::cout << "\nPodaj liczbe z zakresu <1 ; 255>" <<
                              "\n> ";
                    std::cin >> licz;

                    // Walidacja zakresu:
                    if (licz < 0 || licz > 255) {

                        std::cout << "\n\nPodano liczbe spoza akceptowalnego zakresu. Sprobuj ponownie:";

                    } else {

                        // Przypisanie pobranej liczby do zmiennej przechowujacej liczbe do wyslania w kolejnym komunikacie oraz zakonczenie petli:
                        liczba = static_cast<unsigned int>(licz);
                        stop14 = true;
                    }
                }

                // Przygotowanie pozostalych pol komunikatu bedacego proba odgadniecia tajnej liczby:
                odpowiedz = 0;
                zapis(komunikat);

                // Wyslanie komunikatu bedacego proba odgadniecia tajnej liczby:
                if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
                {
                    std::cout << "\n\nBlad gniazda (wysylanie).";
                }
            }

        }
        else if (odpowiedz == 6)
        {
            std::cout << "\n\nOdgadles liczbe! Prosze czekac na wyniki.";

        }
        else if (odpowiedz == 7)
        {
            // Blok kodu obslugujacy kolejna probe odgadniecia tajnej liczby, w przypadku niepowodzenia poprzedniej:
            // Jesli nie otrzymano flagi stopu:
            if (!stop)
            {
                std::cout << "\n\nNie odgadles liczby. Sprobuj ponownie.";

                // Pobieranie od uzytkownika liczby, do pierwszego sukcesu:
                bool stop15 = false;
                while (!stop15) {
                    std::cout << "\nPodaj liczbe z zakresu <1 ; 255>" <<
                              "\n> ";
                    std::cin >> licz;

                    // Walidacja zakresu:
                    if (licz < 0 || licz > 255) {

                        std::cout << "\n\nPodano liczbe spoza akceptowalnego zakresu. Sprobuj ponownie:";

                    } else {

                        // Przypisanie pobranej liczby do zmiennej przechowujacej liczbe do wyslania w kolejnym komunikacie oraz zakonczenie petli:
                        liczba = static_cast<unsigned int>(licz);
                        stop15 = true;
                    }
                }

                // Przygotowanie pozostalych pol komunikatu bedacego kolejna proba odgadniecia tajnej liczby:
                odpowiedz = 0;
                zapis(komunikat);
                if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
                {
                    std::cout << "\n\nBlad gniazda (wysylanie).";
                }
            }
            else
            {
                // Jesli otrzymano flage stopu:
                std::cout << "\n\nNie odgadles liczby. Prosze czekac na wyniki.";
            }
        }

        // Jesli w innym przypadku niz odpowiedz 7. otrzymano flage stopu, zakoncz glowna petle:
        if (stop) break;

    }

    // Odebranie komunikatu z wynikiem:
    if (socket.receive(&komunikat, 3, received, sender, port2) != sf::Socket::Done)
    {
        std::cout << "\n\nBlad gniazda (odbieranie).";
    }

    // Odczyt odebranego komunikatu z wynikiem:
    odczyt(komunikat);

    // Wyswietlenie odpowiedniej informacji o wyniku, w zaleznosci od kodu operacji:
    if (operacja == 4) std::cout << "\n\nGRATULACJE, WYGRALES!";
    else if (operacja == 5) std::cout << "\n\nNIESTETY, PRZEGRALES.";
    else if (operacja == 6) std::cout << "\n\nMAMY REMIS PROSZEEEE PANSTWA";

    // Przygotowanie komunikatu bedacego potwierdzeniem otrzymania wyniku:
    odpowiedz = 1;
    zapis(komunikat);

    // Wyslanie do serwera komunikatu bedacego potwierdzeniem otrzymania wyniku:
    if (socket.send(&komunikat, 3, receiver, port) != sf::Socket::Done)
    {
        std::cout << "\n\nBlad gniazda (wysylanie).";
    }

    return 0;
}